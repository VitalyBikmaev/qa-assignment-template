from behave import given, when, then, step  # pylint: disable=no-name-in-module
from selenium import webdriver

@given(u'i load my test-page "{url}"')
def step_impl(context, url):
    context.url = url
    context.driver.get(context.url) #load page

@then(u'i should see title "{title}"')
def step_impl(context, title):
    context.title = title
    context.testTitle = context.driver.title
    assert context.title == context.testTitle

@then(u'i should see "{listNumber}" default lists')
def step_impl(context, listNumber):
    context.listNumber = listNumber
    assert context.driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[{0}]/div/h2'.format(context.listNumber))

@then(u'i should see header "{header}" in todo default lists')
def step_impl(context, header):
    context.header = header
    assert context.driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[{0}]/div/h2[text() = "{1}"]'.format(context.listNumber,context.header))

##
@when(u'i should see button "{buttonName}" with style parametr "{buttonStyle}"')
def step_impl(context, buttonName, buttonStyle):
    context.buttonName = buttonName
    context.buttonStyle = buttonStyle
    assert context.driver.find_element_by_xpath('//*[@id="{0}"][@style="{1}"]'.format(context.buttonName, context.buttonStyle))

@then(u'i should click on button')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="{0}"][@style="{1}"]/img'.format(context.buttonName, "display: block;")).click()
    
@then(u'i check style parametr')
def step_impl(context):
    assert context.driver.find_element_by_xpath('//*[@id="{0}"][@style="{1}"]'.format(context.buttonName, context.buttonStyle))

@then(u'i should see button OK "{buttonOk}"')
def step_impl(context, buttonOk):
    context.buttonOk = buttonOk
    assert context.driver.find_element_by_xpath('//*[@id="submit_add_todo"]/div[text() = "{0}"]'.format(context.buttonOk))

@then(u'i should see button CANCEL "{buttonCancel}"')
def step_impl(context, buttonCancel):
    context.buttonCancel = buttonCancel
    assert context.driver.find_element_by_xpath('//*[@id="hide_new_todo"]/div[text() = "{0}"]'.format(context.buttonCancel))