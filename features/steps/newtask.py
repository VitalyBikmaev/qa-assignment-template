from behave import given, when, then, step  # pylint: disable=no-name-in-module
from selenium import webdriver

@then(u'i should see button "{buttonName}" without style parametr "display: block;"')
def step_impl(context, buttonName):
    context.buttonName = buttonName
    assert context.driver.find_element_by_xpath('//*[@id="{0}"]'.format(context.buttonName))

@when(u'i should see window "{windowName}" with style parametr "{windowStyle}"')
def step_impl(context, windowName, windowStyle):
    context.windowName = windowName
    context.windowStyle = windowStyle
    assert context.driver.find_element_by_xpath(''.format(context.windowName, context.windowStyle))

@when(u'i should see closed drop-down list "{listName}" with preview title name "{listPreview}"')
def step_impl(context, listName, listPreview):
    context.listName = listName
    context.listPreview = listPreview
    assert context.driver.find_element_by_xpath(''.format(context.listName, context.listStyle))

@when(u'i should see field "{fieldName}" with place holder "{placeHolderName}" and stock value "{valueName}"')
def step_impl(context, fieldName, placeHolderName, valueName):
    context.fieldName = fieldName
    context.placeHolderName = placeHolderName
    context.valueName = valueName
    assert context.driver.find_element_by_xpath(''.format(context.fieldName, context.placeHolderName, context.valueName))

@then(u'i should click button "ОК"')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="submit_add_todo"]/div)')

@then(u'i should see window "create_div" with style parametr "{windowStyle}"')
def step_impl(context, windowStyle):
    context.windowStyle = windowStyle
    assert context.driver.find_element_by_xpath(''.format(context.windowName, context.windowStyle))

@then(u'i should see todo list "{listNumber}" with header "{header}"')
def step_impl(context, listNumber, header):
    context.listNumber = listNumber
    context.header = header

    if context.header == "empty":
        context.header = ""

    assert context.driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[{0}]/div/h2[text() = "{1}"]'.format(context.listNumber,context.header))