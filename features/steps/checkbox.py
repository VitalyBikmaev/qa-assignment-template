from behave import given, when, then, step  # pylint: disable=no-name-in-module
from selenium import webdriver
from parse import *
from parse import compile

@when(u'i should see many check boxes with status "{checkBox}" and id "{checkBoxId}"')
def step_impl(context, checkBox, checkBoxId):
    context.checkBox = checkBox
    context.checkBoxId = checkBoxId
    context.p = compile("input#{}")
    #context.checkBoxArray = [0*i for i in range(len(context.checkBox))]

    context.check1 = context.driver.find_element_by_xpath('//*[@id="{0}"]/div'.format(context.checkBox))
    context.check2 = context.p.parse(str(context.driver.find_element_by_xpath('/html/body/div[3]/div[2]/div[1]/div/form[1]/ul/table/tbody/tr/td[1]/div/input')))
    raise print(context.check1, context.check2, "testText")

@then(u'i have to point at every checkbox')
def step_impl(context):
    pass
    
@when(u'i should see different status "{checkBoxCompare}"')
def step_impl(context, checkBoxCompare):
    pass

@then(u'i should click at every checkbox')
def step_impl(context):
    pass
