Feature: NewTask
#Создаем новую задачу
    Scenario Outline: newTask_1
    #Баг с созданием задачи в категории Семья без ввода данных. Создаётся пустой todo лист
        Given i load my test-page "http://qa-assignment.oblakogroup.ru/board/:andrey_lisyak3"
        Then i should see button "add_new_todo" without style parametr "display: block;"
        And i should click on button
        When i should see window "create_div" with style parametr "display: block;"
        And i should see closed drop-down list "select2-select_category-container" with preview title name "Категория"
        And i should see field "project_text" with place holder "Название задачи..." and stock value "Название задачи"
        Then i should click button "ОК"
        And i should see window "create_div" with style parametr "display: none;"
        And i should see todo list "<number>" with header "<name>"

        Examples: nameOfToDoLists
        | name            | number |
        | СЕМЬЯ           | 1      |
        | РАБОТА          | 2      |
        | ПРОЧЕЕ          | 3      |
        | empty           | 4      |


    #Scenario Outline: newTask_2

    #    Given i load my test-page "http://qa-assignment.oblakogroup.ru/board/:andrey_lisyak2"
    #    Then i should see button "add_new_todo" with style parametr "display: block;"
    #    And i should click on button
    #    When i should see window "create_div" with style parametr "display: block;"
    #    And i should see closed drop-down list "select2-select_category-container" with preview's title name "Категория"
    #    And i should see field "project_text" with place holder "Название задачи..." and stock value "Название задачи"
    #    Then i should click on field "project_text" with place holder "Название задачи..." and stock value "Название задачи"
    #    And i should click button "ОК"
    #    And i should see window "create_div" with style parametr "display: none;"
    #    And i should see todo list "<number>" with header "<name>"

    #    Examples: nameOfToDoLists2
    #    | name            | number |
    #    | СЕМЬЯ           | 1      |
    #    | РАБОТА          | 2      |
    #    | ПРОЧЕЕ          | 3      |
    #    | Название задачи | 4      |
    #    | Название задачи | 5      |