Feature: CheckBox
    Scenario: checkBox_1
        Given i load my test-page "http://qa-assignment.oblakogroup.ru/board/:andrey_lisyak4"
        When i should see many check boxes with status "todo_check" and id "id"
        Then i have to point at every checkbox
        When i should see different status "todo_check"
        Then i should click at every checkbox
        When i should see different status "todo_ckeck"
        Then i have to point at every checkbox
        When i should see different status "todo_ckeck"