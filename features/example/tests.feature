Feature: LoadPage
#Проверка загрузки страницы
  Scenario Outline: loadPage_1
  #Загружаем и смотрим, чтобы на странице были дефолтные todo листы
    Given i load my test-page "http://qa-assignment.oblakogroup.ru/board/:andrey_lisyak2"
    Then i should see title "Task2Project"
    And i should see "<number>" default lists
    And i should see header "<name>" in todo default lists

    Examples: namesOfLists
    #Регистр учитывается (как в дизайне)
    | name   | number |
    | СЕМЬЯ  | 1      |
    | РАБОТА | 2      |
    | ПРОЧЕЕ | 3      |

  Scenario: callWindow
  #Загружаем страницу, нажимаем на кнопку "Добавить новую задачу"
    Given i load my test-page "http://qa-assignment.oblakogroup.ru/board/:andrey_lisyak2"
    When i should see button "add_new_todo" with style parametr "display: block;"
    Then i should click on button
    And i check style parametr
    And i should see button OK "ОК"
    And i should see button CANCEL "ОТМЕНА"
    